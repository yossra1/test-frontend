# Test Component

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 16.1.4.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Use Docker Compose 
Run `docker-compose up`

## Version npde and npm
`node 18.10.0 `
`npm 8.19.2`

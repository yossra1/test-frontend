import { Component, OnInit } from '@angular/core';
// import {debounceTime} from 'rxjs/operators';
import { ErrorHandlerService } from '../../services/error-handler.service';


@Component({
  selector: 'app-flash-message',
  templateUrl: './flash-message.component.html',
  styleUrls: ['./flash-message.component.scss']
})
export class FlashMessageComponent implements OnInit {

  successMessage: string | null = null;

  constructor(private errorHandler: ErrorHandlerService) {

  }
  ngOnInit(): void {

    this.errorHandler.getFlashMessage().subscribe((message) => {
      this.successMessage = message;
      setTimeout(() => { this.successMessage = null; }, 5000);
    });
  }

}


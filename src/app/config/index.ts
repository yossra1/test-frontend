import { environment } from '../../environments/environment';

// default config for dev
let config = {
  apiRoot: 'http://localhost:3000/api/v1/',
  host: 'http://localhost:3000'
};

if (environment.production) {
  config = {
    apiRoot: 'http://test.bsamply.com:3000/api/v1/',
    host: 'http://test.bsamply.com:3000'
  };
}

export { config };

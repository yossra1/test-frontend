import {
    AbstractControl,
    AsyncValidatorFn,
    ValidationErrors,
    ValidatorFn
  } from '@angular/forms';
  import { Observable } from 'rxjs';
  import { map } from 'rxjs/operators';
  import { domainEmailToExculde } from '../shared/config/domainsEmailToExclude';
  import { AccountService } from '../services/account.service';
  
  export class EmailValidator {
  
  
    static checkExistEmail(accountService: AccountService): AsyncValidatorFn {
      return ( control: AbstractControl): Promise<ValidationErrors | null>
  | Observable<ValidationErrors | null> => {
        return accountService.checkEmail(control.value).pipe(
          map(res => {
            return res.email ? null : { emailNotExist: true };
          })
        );
      };
    }
  
    static checkUniqueEmail(accountService: AccountService): AsyncValidatorFn {
      return ( control: AbstractControl ): Observable<ValidationErrors | null> => {
        return accountService.checkEmail(control.value).pipe(
          map(res => {
            return res.email ? { checkUniqueEmail : true } : null;
          })
        );
      };
    }
  

  
    static MatchEmail(AC: AbstractControl): ValidationErrors | null {
      const email = AC.get('email')?.value;
      const emailConfirmation = AC.get('emailConfirmation')?.value;
      if (email !== emailConfirmation && emailConfirmation && email) {
        AC.get('emailConfirmation')?.setErrors({ MatchEmail: true });
      } else {
        return null;
      }
      return null;
    }
  
  
    // static isProfessionnalAndIsValidMailFormat(AC: AbstractControl): ValidationErrors | null {
    //   const EMAIL_REGEXP = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    //   const email = AC.get('email')?.value;
    //   if (email !== '' && (email?.length <= 5  || !EMAIL_REGEXP.test(String(email).toLowerCase()))) {
    //     AC.get('email')?.setErrors({ isValidMailFormat: true });
    //   } else {
    //     const domainToCheck = email.split('@')[1];
    //     const result = domainEmailToExculde.find(domain => {
    //       return domain === domainToCheck;
    //     });
    //     if (result !== undefined) {
    //       AC.get('email')?.setErrors({ isProfessionnal: true });
    //     } else {
  
    //       return null;
    //     }
    //   }
    // }


    static isProfessionnalAndIsValidMailFormat(AC: AbstractControl): ValidationErrors | null {
        const EMAIL_REGEXP = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        const email = AC.get('email')?.value;
        if (email !== '' && (email?.length <= 5 || !EMAIL_REGEXP.test(String(email).toLowerCase()))) {
          AC.get('email')?.setErrors({ isValidMailFormat: true });
        } else {
          const domainToCheck = email?.split('@')[1];
          const result = domainEmailToExculde.find(domain => {
            return domain === domainToCheck;
          });
          if (result !== undefined) {
            AC.get('email')?.setErrors({ isProfessionnal: true });
          } else {
            return null;
          }
        }
        
        return null; // Ajoutez une instruction de retour pour le cas où aucun des cas précédents ne se produit
      }
  
  }
  
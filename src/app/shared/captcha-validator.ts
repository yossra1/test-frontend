import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';
import { CaptchaService } from '../services/captcha.service';


export class CaptchaValidator {

  // static checkCaptcha(captchaService: CaptchaService): ValidatorFn {
  //   let captchaText = '';
  //   return (control: AbstractControl): ValidationErrors | null => {
  //     const captcha = control.get('captcha').value;
  //     captchaService.getCaptchaText().subscribe((res) => {
  //       captchaText = res;
  //     });
  //     if (captchaText === captcha) {
  //       return null;
  //     } else { control.get('captcha').setErrors({ checkCaptcha: true }); }


  //   };
  // }

  static checkCaptcha(captchaService: CaptchaService): ValidatorFn {
    let captchaText = '';
    return (control: AbstractControl): ValidationErrors | null => {
      if (control) {
        const captcha = control.get('captcha')?.value;
        captchaService.getCaptchaText().subscribe((res) => {
          captchaText = res;
        });
        if (captchaText === captcha) {
          return null;
        } else {
          control.get('captcha')?.setErrors({ checkCaptcha: true });
        }
      }
      return null;
    };
  }

}

export class ProductType {
    _id!: string;
    name!: string;
    description!: string;
    marketplace!: string; // ref Id to MarketPLace
}

export class ProductTypeList {
    docs!: ProductType[];
    total!: number;
    limit!: number;
    page!: number;
    pages!: number;
}

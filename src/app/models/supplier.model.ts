import { MembershipPlan } from './membership-plan.model';

export class Contract {
  name!: string;
  isSigned!: boolean;
  file!: string;
}
export class Membership {
  plan: MembershipPlan;
  isPaid: boolean;
  constructor() {
    this.plan = new MembershipPlan();
    this.isPaid = false;
  }
}
export class Supplier {
  _id!: string;
  image: string;
  status: string;

  // name is the name of Supplier
  name: string;

  email: string;
  posts: number;
  country: string;
  city: string;
  zipCode!: number;
  address: string;
  phoneNumber: string;
  website: string;
  foundedIn: number;
  productionDistrict: string;
  numberOfOmployees: number;
  majorClients: string;
  mainMarkets: string;
  designsInCollections: number;
  designsInArchive: number;
  monthlyProductionCapacity: number;
  certifications: string;
  tradeShows: string;
  campanyDescription: string;
  disputeDelay: number;
  courtOfjuridiction: string;
  temporaryStockConditions: number;
  additionalComments: string;
  rawMaterials: string[];
  contract!: Contract;
  membershipLevel: string;
  products: number;
  billingAdress: string;
  membership!: Membership;
  fee: string;
  paymentMethod: string;
  expires: string;
  applicationDate: string;
  agent!: string;
  taxID: string;
  profileStatus: string;
  supplier!: Supplier;
  constructor(init?: Partial<Supplier>) {
    this.image = '';
    this.status = '';
    this.name = '';
    this.email = '';
    this.country = '';
    this.posts = 0;
    this.city = '';
    this.address = '';
    this.website = '';
    this.name = '';
    this.foundedIn = 0;
    this.productionDistrict = '';
    this.numberOfOmployees = 0;
    this.majorClients = '';
    this.mainMarkets = '';
    this.designsInCollections = 0;
    this.designsInArchive = 0;
    this.monthlyProductionCapacity = 0;
    this.certifications = '';
    this.tradeShows = '';
    this.campanyDescription = '';
    this.disputeDelay = 0;
    this.courtOfjuridiction = '';
    this.temporaryStockConditions = 0;
    this.additionalComments = '';
    this.rawMaterials = [];
    this.membershipLevel = '';
    this.products = 0;
    this.billingAdress = '';
    // this.membership = new Membership();
    this.fee = '';
    this.paymentMethod = '';
    this.expires = '';
    this.applicationDate = '';
    this.taxID = '';
    this.phoneNumber = '';
    // this.agent = '';
    this.profileStatus = '';
    Object.assign(this, init);
  }

}

export class SupplierAccount {
  email: string;
  emailConfirmation: string;
  firstName: string;
  lastName: string;
  password: string;

  constructor() {
    this.email = '';
    this.emailConfirmation = '';
    this.firstName = '';
    this.lastName = '';
    this.password = '';
  }

}

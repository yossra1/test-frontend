export class ProductAvailabilityInformations {
    name!: string;
    domesticMarket!: string;
    brandFromSchengenCountries!: string;
    extraUeBrand!: string;
}



export class Agreement {

    _id: string;
    productAvailabilityInformations: Array<ProductAvailabilityInformations>;
    file: string;
    supplier: string;
    constructor() {
        this._id = '';
        this.productAvailabilityInformations = new Array<ProductAvailabilityInformations>();
        this.file = '';
        this.supplier = '';
    }

}
export class AgreementContainer {
  docs: Agreement[];
  total: string;
  limit: string;
  page: string;
  pages: string;

  constructor() {
    this.docs = [];
    this.total = "";
    this.limit = "";
    this.page = "";
    this.pages = "";
  }
}

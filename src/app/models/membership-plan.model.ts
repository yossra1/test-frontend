class OnlineProducts {
    value!: number;
    description!: string;
}
class BulkUpload {
    value!: boolean;
    description!: string;
}
class IsMultiAccount {
    value!: boolean;
    description!: string;
}
class Lightbox {
    value!: boolean;
    description!: string;
}

class OnBoardingAndSetup {
    value!: boolean;
    description!: string;
}
class MobileAppQRCode {
    value!: string;
    description!: string;
}
class DataPack {
    title!: string;
    value!: boolean;
    description!: string;
}
class SearchEngine {
    value!: boolean;
    description!: string;
}
class PriorityOnViews {
    value!: boolean;
    description!: string;
}
class IntegrationPack {
    value!: boolean;
    description!: string;
}
class Price {
    value!: number;
    description!: string;
}
export class Commissions {
    stock!: string;
    value!: number;
}
class SampleCommission {
    value!: number;
    description!: string;
    title!: string;
}
export class MembershipPlan {
    _id!: string;
    name!: string;
    description!: string;
    onlineProducts!: OnlineProducts;
    bulkUpload!: BulkUpload;
    isMultiAccount!: IsMultiAccount;
    lightbox!: Lightbox;
    onBoardingAndSetup!: OnBoardingAndSetup;
    mobileAppQRCode!: MobileAppQRCode;
    dataPack!: DataPack;
    searchEngine!: SearchEngine;
    priorityOnViews!: PriorityOnViews;
    integrationPack!: IntegrationPack;
    price!: Price;
    commissions!: Array<Commissions>;
    sampleCommission!: SampleCommission;
    docs!: any[];
    constructor() {
        this._id = '';
        this.name = '';
        this.description = '';
        this.onlineProducts = new OnlineProducts();
        this.bulkUpload = new BulkUpload();
        this.isMultiAccount = new IsMultiAccount();
        this.lightbox = new Lightbox();
        this.onBoardingAndSetup = new OnBoardingAndSetup();
        this.mobileAppQRCode = new MobileAppQRCode();
        this.dataPack = new DataPack();
        this.searchEngine = new SearchEngine();
        this.priorityOnViews = new PriorityOnViews();
        this.integrationPack = new IntegrationPack();
        this.price = new Price();
        this.commissions = new Array<Commissions>();
        this.sampleCommission = new SampleCommission();
    }
}

export class ProductAvailabilityInformations {
    stock!: string;
    name!: string;
    priceRangeMin!: string;
    priceRangeMax!: string;
    shipsFrom!: string;
    sampleAvailability!: string;
    minimumOrderQty!: string;
    averageLeadTime!: string;
    materials!: string;
    endUse!: string;

}
export class Division {
    _id!: string;
    name!: string;
    productTypes!: Array<string>;
    marketplaces!: Array<string>;
    productAvailabilityInformations!: Array<ProductAvailabilityInformations>;
    supplier!: string;

    constructor(init?: Partial<Division>) {
        this.name = '';
        this.productAvailabilityInformations = new Array<ProductAvailabilityInformations>();
        this.supplier = '';
        Object.assign(this, init);
    }
}

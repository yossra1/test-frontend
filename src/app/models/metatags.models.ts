export interface MetaTags {
  title: string;
  description: string;
  image: string;
  route: string;
  }
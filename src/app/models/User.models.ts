import { Supplier } from './supplier.model';

export class TeamMembers {
  docs!: User[];
  _id!: string;
}

class Role {
  name!: string;
  capabilities?: string[];
  division?: string;
  country?: string;
  team?: string;
}

export class User {
  _id!: string;
  email: string;
  firstName: string;
  lastName: string;
  brand!: string;
  role: Role;
  createdAt!: string;
  updatedAt!: string;
  status!: string;
  roleAtCompany: string;
  supplier!: Supplier; // ref ID to Supplier
  password: string;

  constructor(init?: Partial<User>) {
    this.email = '';
    this.firstName = '';
    this.lastName = '';
    this.roleAtCompany = '';
    this.role = new Role();
    this.password = '';
    Object.assign(this, init);
  }
}





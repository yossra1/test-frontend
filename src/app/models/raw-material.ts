import { ProductType } from './product-type.model';

export class RawMaterial {
  _id: string;
  name: string;
  productTypes: ProductType[];
  docs: any;
  constructor() {
    this._id = '';
    this.name = '';
    this.productTypes = [];
  }
}

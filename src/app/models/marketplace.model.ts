export class Marketplace {
  _id!: string;
  name!: string;
  description!: string;
  status!: string;
}

export class MarketplaceList {
  docs!: Marketplace[];
  total!: number;
  limit!: number;
  page!: number;
  pages!: number;
}

import { Injectable } from '@angular/core';
import { Meta } from '@angular/platform-browser';
import { Title } from '@angular/platform-browser';
import { config } from '../config';
// import { MetaTags } from '../models/metatags.models';

@Injectable({
  providedIn: 'root'
})



export class SeoService {

  constructor(private meta: Meta, private titleService: Title) { }
  
  generateTags(metaTags : any ) {
    metaTags = {
      title: 'Home',
      description: 'The B2B fashion platform',
      image: '',
      route: '',
      ...metaTags
    };
    this.titleService.setTitle(metaTags.title + ' - Bsamply');

    this.meta.updateTag({ name: 'twitter:card', content: 'summary' });
    this.meta.updateTag({ name: 'twitter:site', content: 'Bsamply' });
    this.meta.updateTag({ name: 'twitter:title', content: metaTags.title + ' - Bsamply' });
    this.meta.updateTag({ name: 'twitter:description', content: metaTags.description });
    this.meta.updateTag({ name: 'twitter:image', content: metaTags.image });

    this.meta.updateTag({ property: 'og:type', content: 'article' });
    this.meta.updateTag({ property: 'og:site_name', content: 'Bsamply' });
    this.meta.updateTag({ property: 'og:title', content: metaTags.title + ' - Bsamply' });
    this.meta.updateTag({ property: 'og:description', content: metaTags.description });
    this.meta.updateTag({ property: 'og:image', content: metaTags.image });
    this.meta.updateTag({ property: 'og:url', content: config.host + metaTags.route });
  }
}

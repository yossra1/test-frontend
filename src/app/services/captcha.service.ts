import { HttpErrorResponse, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError, Subject, Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { config } from '../../app/config';

@Injectable({
  providedIn: 'root'
})
export class CaptchaService {

path = config.apiRoot;
  private subjectCaptchaText: Subject<string> = new Subject<string>();

  constructor(private http: HttpClient) { }

  get() {
    return this.http.get<any>(this.path + 'captcha/')
      .pipe(map(data => {
        this.subjectCaptchaText.next(data.captchaText);
        return data;
      }))
      .pipe(catchError((err: HttpErrorResponse) => {
        return throwError(err);
      }));
  }


  getCaptchaText(): Observable<string> {
    return this.subjectCaptchaText.asObservable();
  }
}

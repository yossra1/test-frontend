import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { config } from '../config';
import { User } from '../models/User.models';

@Injectable({
  providedIn: 'root'
})

export class AccountService {
  apiUrl = config.apiRoot;

  constructor(private http: HttpClient) { }

  checkEmail(email: string): Observable<any> {
    return this.http.get(this.apiUrl + 'accounts/checkEmail?email=' + email)
    .pipe(catchError((err: HttpErrorResponse) => {
      return throwError(err);
    }));
  }


  // send token to user by email on forgot password
  sendActivationKey(email: string): Observable<any> {
    return this.http.put<any>(this.apiUrl + 'accounts/forgotPassword/', { 'email': email })
      .pipe(catchError((err: HttpErrorResponse) => {
        return throwError(err);
      }));
  }

  // send token to user by email on forgot password
  resendActivationLink(email: string): Observable<any> {
    return this.http.post<any>(this.apiUrl + 'accounts/resendActivation/', { 'email': email });
  }

  resetPassword(user: any, token: string, email: string): Observable<any> {
    return this.http.put<any>(this.apiUrl + 'accounts/resetPassword?token=' + token + '&email=' + email, { 'user': user })
      .pipe(catchError((err: HttpErrorResponse) => {
        return throwError(err);
      }));
  }

 confirmAccount(email: string, token: string): Observable<any> {
   console.log(email, token);

    return this.http.post(this.apiUrl + 'accounts/confirmAccount?token=' + token + '&email=' + email, '')
      .pipe(catchError((err: HttpErrorResponse) => {
        return throwError(err);
      }));

  }

  checkUserByTokenAndEmail(email: string, token: string): Observable<any> {
    return this.http.get(this.apiUrl + 'accounts/checkTokenAndEmail?token=' + token + '&email=' + email)
    .pipe(catchError((err: HttpErrorResponse) => {
      return throwError(err);
    }));
  }

  update(_id: string, dataUser: User): Observable<any> {
    return this.http.put<User>(this.apiUrl + 'users/' + _id, { 'user': dataUser })
      .pipe(catchError((err: HttpErrorResponse) => {
        return throwError(err);
      }));

  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { config } from '../config';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  path = config.apiRoot;

  constructor(private http: HttpClient) { }

  getSetting(key: string) {
    return this.http.get<any>(this.path + 'options?key=' + key);
  }

}

import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StepperService {
  private subject: Subject<number> = new Subject<number>();

  constructor() { }

  setStepIndex(index: number) {
    this.subject.next(index);
  }

  getCurrentStep(): Observable<number> {
    return this.subject.asObservable();
  }
}

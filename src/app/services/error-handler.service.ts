import { Injectable } from '@angular/core';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ErrorHandlerService {

  private _success = new Subject<string>();

  constructor() { }

  setFlashMessage(data: { reason: string; }) {
    return this._success.next(data.reason);
  }

  getFlashMessage(): Subject<string> {
    return this._success;
  }
}

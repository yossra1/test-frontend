import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { User } from '../models/User.models';
import { config } from '../config';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  path = config.apiRoot;
  private connectedUser$: BehaviorSubject<User | null> = new BehaviorSubject<User | null>(null);
  constructor(private http: HttpClient) { }

  // authenticate(): Observable<User> {
  //   return this.http.get<User>(this.path + 'auth/me')
  //     .pipe(map((res: any) => {
  //       this.connectedUser$.next(res.user);
  //       return res.user;
  //     }))
  //     .pipe(catchError(
  //       () => of('error')
  //     ));
  // }

  authenticate(): Observable<User> {
    return this.http.get<User>(this.path + 'auth/me')
      .pipe(
        map((res: any) => {
          this.connectedUser$.next(res.user);
          return res.user;
        }),
        catchError(() => {
          return of(null); // Return null or appropriate default value in case of error
        })
      ) as Observable<User>; // Cast the result as Observable<User>
  }

  login(email: string, password: string) {

    return this.http.post<any>(this.path + 'auth/login', { email, password })
      .pipe(map(token => {
        // login successful if there's a jwt token in the response
        if (token && token.token) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('token', token.token);
          this.authenticate().subscribe();
        }

        return token;
      }));
  }

  getConnectedUser(): BehaviorSubject<User | null> {
    return this.connectedUser$;
  }

  // logout() {
  //   return this.http.post<any>(this.path + 'auth/logout', null)
  //     .pipe(map(() => {
  //       // remove user from local storage to log user out
  //       localStorage.removeItem('token');
  //       this.connectedUser$.next(null);
  //     }))
  //     .pipe(catchError((error: HttpErrorResponse) => {
  //       if (error.error.message === 'No token provided.') {
  //         this.connectedUser$.next(null);
  //       }
  //       return of(error);
  //     }));
  // }

  logout() {
    console.log('logout')
  }

  forceLogout() {
    localStorage.removeItem('token');
    // this.connectedUser$.next(null);
    console.log('forceLogout')

  }

}

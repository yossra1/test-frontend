import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { config } from '../../config';
import { Supplier } from 'src/app/models/supplier.model';
import { User } from 'src/app/models/User.models';

@Injectable({
  providedIn: 'root'
})
export class SupplierService {

  apiUrl = config.apiRoot;
  private subjectEmail: Subject<string> = new Subject<string>();
  private subjectMemberShipLevel: Subject<string> = new Subject<string>();

  constructor(private http: HttpClient) { }

  registerSupplier(dataSupplier: any): Observable<Supplier> {
    return this.http.post<any>(this.apiUrl + 'accounts/register/supplier', dataSupplier);
  }

  getById(_id: string): Observable<Supplier> {
    return this.http.get<Supplier>(this.apiUrl + 'suppliers/' + _id)
      .pipe(map(data => {
        return data;
      }))
      .pipe(catchError((err: HttpErrorResponse) => {
        return throwError(err);
      }));
  }

  update(_id: string, dataSupplier: Supplier): Observable<any> {
    return this.http.put<Supplier>(this.apiUrl + 'suppliers/' + _id, { 'supplier': dataSupplier })
      .pipe(map(data => {
        return data;
      }))
      .pipe(catchError((err: HttpErrorResponse) => {
        return throwError(err);
      }));
  }

  setEmail(email: string) {
    this.subjectEmail.next(email);

  }

  getEmail(): Observable<string> {
    return this.subjectEmail.asObservable();
  }


  setMemberShipLevel(memberShipName: string) {
    this.subjectMemberShipLevel.next(memberShipName);
  }

  getMemberShipLevel() {
    return this.subjectMemberShipLevel.asObservable();
  }

  /**return contrcat supplier
   * @_id:id supplier */
  getContract(_id: string) {
    return this.http.get<any>(this.apiUrl + 'suppliers/' + _id)
      .pipe(map(data => {
        return data;
      }))
      .pipe(catchError((err: HttpErrorResponse) => {
        return throwError(err);
      }));
  }


  /** @_id :id supplier to sign contract
  // tslint:disable-next-line: no-redundant-jsdoc
   *@name:supplier name who sign the contract
   */
  signContract(_id: string, name: string) {
    return this.http.put(this.apiUrl + 'suppliers/' + _id + '/signContract', { 'name': name })
      .pipe(map(data => {
        return data;
      }))
      .pipe(catchError((err: HttpErrorResponse) => {
        return throwError(err);
      }));
  }

  /** @_id :id supplier
  *@membershipPlan:membershipPlan name
  */
  updateMemberShipPlan(_id: string, membershipPlan: string) {
    return this.http.put(this.apiUrl + 'suppliers/' + _id + '/membershipPlan', { 'plan': membershipPlan })
      .pipe(map(data => {
        return data;
      }))
      .pipe(catchError((err: HttpErrorResponse) => {
        return throwError(err);
      }));
  }

  /**
   * Get team members list
   */
  getMembers(_idSupplier: string, page:number): Observable<any> {
    return this.http.get<User>(this.apiUrl + 'suppliers/' + _idSupplier + '/members?page=' + page);
  }

  /**
   * Get Memver by id
   * @param _idMember
   */
  getMemberById(_idMember: string): Observable<any> {
    return this.http.get<User>(this.apiUrl + 'users/' + _idMember);
  }

  /**
   * add Member
   */
  addMember(_idSupplier: string, memberData: any) {
    return this.http.post(this.apiUrl + 'suppliers/' + _idSupplier + '/member', memberData)
      .pipe(map(data => {
        return data;
      }))
      .pipe(catchError((error: HttpErrorResponse) => {
        return throwError(error);
      }));
  }

  /**
   * edit Member
   */
  editMember(_idMember: string, memberData: any) {
    return this.http.put(this.apiUrl + 'users/' + _idMember, memberData)
      .pipe(map(data => {
        return data;
      }))
      .pipe(catchError((error: HttpErrorResponse) => {
        return throwError(error);
      }));
  }

  /**
   * delete Member
   */
  deleteMember(_idMember: string) {
    return this.http.delete(this.apiUrl + 'users/' + _idMember)
      .pipe(map(data => {
        return data;
      }))
      .pipe(catchError((error: HttpErrorResponse) => {
        return throwError(error);
      }));
  }

}

import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { config } from '../../config';
import { Division } from '../../models/division.model';

@Injectable({
    providedIn: 'root'
})
export class DivisionService {
    apiUrl = config.apiRoot;
    constructor(private http: HttpClient) { }

    getAll(_idSupplier: string): Observable<any> {
        return this.http.get<Division>(this.apiUrl + 'suppliers/' + _idSupplier + '/divisions/');
    }

    getById(_idSupplier: string, _idDivision: string): Observable<any> {
        return this.http.get<Division>(this.apiUrl + 'suppliers/' + _idSupplier + '/divisions/' + _idDivision);
    }

    add(dataDivision: any): Observable<any> {
        return this.http.post<any>(this.apiUrl + 'suppliers/divisions', dataDivision)
            .pipe(map(data => {
                return data;
            }))
            .pipe(catchError((err: HttpErrorResponse) => {
                return throwError(err);
            }));
    }

    update(_idSupplier: string, _idDivision: string, dataDivision: any): Observable<any> {
        return this.http.put<any>(this.apiUrl + 'suppliers/' + _idSupplier + '/divisions/' + _idDivision, dataDivision)
            .pipe(map(data => {
                return data;
            }))
            .pipe(catchError((error: HttpErrorResponse) => {
                return throwError(error);
            }));
    }

    delete(_idSupplier: string, _idDivision: string): Observable<any> {
        return this.http.delete<any>(this.apiUrl + 'suppliers/' + _idSupplier + '/divisions/' + _idDivision);
    }

    getMarketplace(): Observable<any> {
        return this.http.get(this.apiUrl + 'marketplaces/');
    }

    getMarketplaceById(_id: string): Observable<any> {
        return this.http.get(this.apiUrl + 'marketplaces/' + _id);
    }

    getProductType(_id: string): Observable<any> {
        return this.http.get(this.apiUrl + 'productTypes/' + _id);
    }

    getStock(): Observable<any> {
        return this.http.get(this.apiUrl + 'stock');
    }



}

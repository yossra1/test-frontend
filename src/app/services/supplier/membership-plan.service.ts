import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { config } from '../../config';
import { MembershipPlan } from '../../models/membership-plan.model';


@Injectable({
  providedIn: 'root'
})
export class MembershipPlanService {

  apiUrl = config.apiRoot;

  constructor(private http: HttpClient) { }

  getAll(): Observable<any> {
    return this.http.get<any>(this.apiUrl + '/plan');
  }

  getById(_id: string): Observable<MembershipPlan> {
    return this.http.get<MembershipPlan>(this.apiUrl + '/plan' + _id);
  }

}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { config } from '../../config';
import { AgreementContainer } from '../../models/agreement.model';

@Injectable({
  providedIn: 'root'
})
export class AgreementService {
  apiUrl = config.apiRoot;
  constructor(private http: HttpClient) {}

  getAgreements(_idSupplier: any, page:any): Observable<AgreementContainer> {
    return this.http.get<AgreementContainer>(
      this.apiUrl + 'suppliers/' + _idSupplier + '/agreements?page=' + page
    );
  }
}

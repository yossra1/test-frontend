import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../../models/User.models';
import { AuthService } from 'src/app/services/auth.service';
import { SeoService } from 'src/app/services/seo.service';

// import { RedirectService } from '../../../../shared/services/redirect.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  auth: any = {};
  public email = '';
  public password = '';
  currentUser!: User;
  urlToRedirect = '/';

  constructor(
    public authService: AuthService,
    public seoService: SeoService,
    private router: Router,
    // private redirectService: RedirectService
  ) { }

  ngOnInit() {
    this.seoService.generateTags({
      title: 'Log in',
      route: '/login'
    });
  }

  async onSubmit() {

    // Why does the wrong email/password error return with an 401 (authorization) status code
    await this.authService.login(this.auth.email, this.auth.password).toPromise().then().catch(
      err => {}
    );
    this.authService.getConnectedUser().subscribe(
      user => {
        if (user) {
          this.currentUser = user;
          this.redirectUser();
        }
      });
  }

  redirectUser() {
    // if (this.currentUser.supplier && this.currentUser.role.team === 'supplier') {
    //   const urlToREdirect = this.redirectService.redirectSupplier(this.currentUser);
    //   this.router.navigate([urlToREdirect]);

    // } else {
    //   this.router.navigate(['/']);
    // }
    console.log("redirectUser");
  }
}

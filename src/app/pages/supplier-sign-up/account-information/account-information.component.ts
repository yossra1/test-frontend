import { isPlatformBrowser } from '@angular/common';
import { Component, HostListener, Inject, OnDestroy, OnInit, PLATFORM_ID } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Supplier, SupplierAccount } from 'src/app/models/supplier.model';
import { StepperService } from 'src/app/services/stepper.service';
import { SupplierService } from 'src/app/services/supplier/supplier.service';
import { DialogService } from 'src/app/services/dialog.service';
import { CaptchaService } from 'src/app/services/captcha.service';
import { CaptchaValidator } from 'src/app/shared/captcha-validator';



@Component({
  selector: 'app-account-information',
  templateUrl: './account-information.component.html',
  styleUrls: ['./account-information.component.scss']
})

export class AccountInformationComponent implements OnInit, OnDestroy {

  accountInformationForm!: FormGroup;
  accountSupplier: SupplierAccount = new SupplierAccount();
  destroySubject$: Subject<boolean> = new Subject<boolean>();
  supplier: Supplier = new Supplier();
  captchaImage!: SafeHtml;
  loading = false;
  valueBtnNext = 'Next step';
  public windowWidth: number = window.innerWidth;

  constructor(private formBuilder: FormBuilder,
    @Inject(PLATFORM_ID) private platform: Object,
    private _stepperService: StepperService,
    private supplierService: SupplierService,
    private dialogService: DialogService,
    private captchaService: CaptchaService,
    public sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.createForm();
    this.getCaptcha();
  }

  createForm() {
    this.accountInformationForm = this.formBuilder.group({
      companyName: ['', Validators.required],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', Validators.required],
      emailConfirmation: ['', Validators.required],
      password: ['', Validators.required],
      captcha: [''],
      agreePrivacyPolicy: ['', Validators.required]
    }, {
        validator: [
          CaptchaValidator.checkCaptcha(this.captchaService)
        ]
      }
    );
  }

  // convenience getter for easy access to form fields
  get f() { return this.accountInformationForm.controls; }

  goNext() {
    this._stepperService.setStepIndex(2);
  }

  getCaptcha() {
    this.captchaService.get()
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((res) => {
      this.captchaImage = this.sanitizer.bypassSecurityTrustHtml(res.captchaSVG);
    });

  }

  refreshCaptchaService() {
    this.getCaptcha();
  }
  applySignupSupplier() {
    this.loading = true;
    this.valueBtnNext = 'Loading...';
    this.supplier.email = this.accountSupplier.email;
    const dataSupplier = {
      'user': { ...this.accountSupplier },
      'supplier': { ...this.supplier }
    };

    setTimeout(() => this.supplierService.registerSupplier(dataSupplier)
      .pipe(takeUntil(this.destroySubject$))
      .subscribe(
        () => {

          this._stepperService.setStepIndex(2);
          this.supplierService.setEmail(this.supplier.email);
        },
        () => {
          // works only on client side and not in server side
          if (isPlatformBrowser(this.platform)) {
            window.scrollTo({ left: 0, top: 100, behavior: 'smooth' });
          }
          this.loading = false;
          this.valueBtnNext = 'Next step';
        }), 400);

  }

  canDeactivate(): Observable<boolean> | boolean {
    if (this.accountInformationForm.dirty) {

      return this.dialogService.confirm('Discard changes for Supplier?');
    }
    return true;
  }
  
  // if screen size changes it'll update
  @HostListener('window:resize', ['$event'])
  resize(event:Event) {
    this.windowWidth = window.innerWidth;
  }
  ngOnDestroy() {
    this.destroySubject$.next(true);
    this.destroySubject$.unsubscribe();
  }

}

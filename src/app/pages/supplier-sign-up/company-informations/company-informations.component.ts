import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { SupplierService } from '../../../services/supplier/supplier.service';
import { Supplier } from 'src/app/models/supplier.model';
import { User } from 'src/app/models/User.models';
import { AccountService } from 'src/app/services/account.service';
import { AuthService } from 'src/app/services/auth.service';
import { StepperService } from 'src/app/services/stepper.service';
import { CountriesService } from 'src/app/services/supplier/countries.service';

@Component({
  selector: 'app-company-informations',
  templateUrl: './company-informations.component.html',
  styleUrls: ['./company-informations.component.scss']
})
export class CompanyInformationsComponent implements OnInit, OnDestroy {
  companyInformationForm!: FormGroup;
  user: User = new User();
  supplier: Supplier = new Supplier();
  countries: string[] = [];
  destroySubject$: Subject<boolean> = new Subject<boolean>();
  loading = false;
  valueBtnNext = 'Next step';
  countriesData: any;
  //  states!: {};
  // states: string[] = [];
  states: any[] = [];


  statesData= [];

  constructor(
    private formBuilder: FormBuilder,
    public _AuthService: AuthService,
    private supplierService: SupplierService,
    private accountService: AccountService,
    private _stepperService: StepperService,
    private router: Router,
    private countriesService: CountriesService
  ) {
    this.countriesService
      .getCountries()
      .pipe(takeUntil(this.destroySubject$))
      .subscribe(async countriesData => {
        this.countriesData = countriesData;
        this.countries = Object.keys(this.countriesData).map(function(key) {
          return  countriesData[key].name ;
        });
      });
  }

  ngOnInit() {
    this._stepperService.setStepIndex(3);
    this._AuthService
      .getConnectedUser()
      .pipe(takeUntil(this.destroySubject$))
      .subscribe(res => {
        this.user = new User(res ?? {});
        this.supplier = new Supplier(this.user.supplier);
      });

    this.createForm();
  }

  createForm() {
    this.companyInformationForm = this.formBuilder.group({
      companyName: ['', Validators.required],
      country: ['', Validators.required],
      address: [''],
      companyWebSite: [''],
      taxID: ['', Validators.required],
      cityProvince: [''],
      zipCode: [''],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      phoneNumber: ['', Validators.required],
      roleAtCompany: [null, Validators.required]
    });
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.companyInformationForm.controls;
  }

  // goNext() {
  //   if (this.companyInformationForm.invalid) {
  //     return;
  //   }
  //   this.loading = true;
  //   this.valueBtnNext = 'Loading...';
  //   setTimeout(
  //     () =>
  //       this.accountService
  //         .update(this.user._id, this.user)
  //         .pipe(takeUntil(this.destroySubject$))
  //         .subscribe(res => {
  //           this.user = res.user;
  //           this.supplierService
  //             .update(this.supplier._id, this.supplier)
  //             .pipe(takeUntil(this.destroySubject$))
  //             .subscribe(data => {
  //               this.supplier = data.supplier;
  //               this.loading = false;

  //               this.router.navigate(['supplier-sign-up/materials']);
  //               this._stepperService.setStepIndex(4);
  //             });
  //         }),
  //     400
  //   );
  // }

  goNext() {
    this._stepperService.setStepIndex(4);
  }

  goPrev() {
     this._stepperService.setStepIndex(2);
  }

  getCities(countryName: string) {

      Object.keys(this.countriesData).find((key , index) => {
      if (this.countriesData[key].name === countryName) {
         this.statesData = this.countriesData[key].states;
         return true;
      }
      return false; // Return false when the condition is not satisfied

    } );
    this.states = Object.values(this.statesData).map(function (state:{ name: string }) {
      return state.name;

    });

  }


  ngOnDestroy() {
    this.destroySubject$.next(true);
    this.destroySubject$.unsubscribe();
  }
}

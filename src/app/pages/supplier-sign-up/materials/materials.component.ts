import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Marketplace } from 'src/app/models/marketplace.model';
import { RawMaterial } from 'src/app/models/raw-material';
import { Supplier } from 'src/app/models/supplier.model';
import { User } from 'src/app/models/User.models';
import { AccountService } from 'src/app/services/account.service';
import { AuthService } from 'src/app/services/auth.service';
import { StepperService } from 'src/app/services/stepper.service';
import { SupplierService } from 'src/app/services/supplier/supplier.service';
// import { MarketplaceService } from '../../../../../modules/products/services/marketplace.service';

@Component({
  selector: 'app-materials',
  templateUrl: './materials.component.html',
  styleUrls: ['./materials.component.scss']
})
export class MaterialsComponent implements OnInit, OnDestroy {
  marketplaces!: Marketplace[];
  rawMaterials: RawMaterial[] = [];
  selectedRawMaterials: string[] = [];
  isStepValid = false;
  public isCollapsedFashion = false;
  public isCollapsedInteriors = false;
  destroySubject$: Subject<boolean> = new Subject<boolean>();
  public isClicked: boolean[] = [];
  user: User = new User();
  supplier: Supplier = new Supplier();
  loading = false;
  valueBtnNext = 'Next step';
  constructor(
    private _stepperService: StepperService,
    // private marketplaceService: MarketplaceService,
    public _AuthService: AuthService,
    private supplierService: SupplierService,
    private accountService: AccountService,
    private router: Router
  ) {}

  ngOnInit() {
    // this._AuthService
    //   .getConnectedUser()
    //   .pipe(takeUntil(this.destroySubject$))
    //   .subscribe(res => {
    //     this.user = res;
    //     this.supplier = this.user.supplier;
    //   });

    this.getRawMaterials();
  }

  Onchange(e:Event, id:string) {
    if (e.target && (e.target as HTMLInputElement).checked) {
      this.selectedRawMaterials.push(id);
    } else {
      this.selectedRawMaterials.splice(
        this.selectedRawMaterials.indexOf(id),
        1
      );
    }
    if (this.selectedRawMaterials && this.selectedRawMaterials.length) {
      this.isStepValid = true;
    } else {
      this.isStepValid = false;
    }
  }

  isRawMaterialChecked(idToCheck: string) {
    return this.selectedRawMaterials.find(id => id === idToCheck);
  }

  getRawMaterials() {
    // this.marketplaceService
    //   .getMarketplaces(1, Math.pow(2, 32))
    //   .pipe(takeUntil(this.destroySubject$))
    //   .subscribe(data => {
    //     this.marketplaces = data.docs;
    //     this.getMarketplaceProductTypes();
    //   });
  }

  getMarketplaceProductTypes() {
    let rawMaterial: RawMaterial;
    // for (const marketplace of this.marketplaces) {
    //   this.marketplaceService
    //     .getMarketplaceProductTypes(marketplace._id, 1, Math.pow(2, 32))
    //     .pipe(takeUntil(this.destroySubject$))
    //     .subscribe(data => {
    //       rawMaterial = new RawMaterial();
    //       rawMaterial.name = marketplace.name;
    //       rawMaterial.productTypes = data.docs;
    //       this.rawMaterials.push(rawMaterial);
    //       this.isClicked.push(false);
    //     });
    // }
    this.isClicked[1] = true;
  }

  // get productType Items
  // getItems(groupName: string) {
  //   return this.rawMaterials.find(item => item.name === groupName).productTypes;
  // }

  getItems(groupName: string) {
    const item = this.rawMaterials.find(item => item.name === groupName);
    if (item) {
      return item.productTypes;
    }
    return [];
  }

  // goNext() {
    // if (!this.isStepValid) {
    //   return;
    // }
    // this.loading = true;
    // this.valueBtnNext = 'Loading...';
    // this.supplier.rawMaterials = this.selectedRawMaterials;
    // this.user.status = 'confirmed';
    // setTimeout(
    //   () =>
    //     this.accountService
    //       .update(this.user._id, this.user)
    //       .pipe(takeUntil(this.destroySubject$))
    //       .subscribe(res => {
    //         this.user = res.user;
    //         this.supplierService
    //           .update(this.supplier._id, this.supplier)
    //           .pipe(takeUntil(this.destroySubject$))
    //           .subscribe(data => {
    //             this.loading = false;
    //             this.supplier = data.supplier;
    //             // console.log(data.supplier);
    //             this.router.navigate(['supplier-sign-up/profile-status']);
    //           });
    //       }),
    //   400
    // );
    // this.router.navigate(['supplier-sign-up/profile-status']);
  // }

  goNext() {
    this._stepperService.setStepIndex(5);
  }

  goPrev() {
    // this.router.navigate(['supplier-sign-up/company-information']);
    this._stepperService.setStepIndex(3);
  }

  ngOnDestroy() {
    this.destroySubject$.next(true);
    this.destroySubject$.unsubscribe();
  }
}

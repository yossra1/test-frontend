import { Component, Input, OnInit } from '@angular/core';
import { StepperService } from 'src/app/services/stepper.service';

@Component({
  selector: 'app-account-verification',
  templateUrl: './account-verification.component.html',
  styleUrls: ['./account-verification.component.scss']
})
export class AccountVerificationComponent implements OnInit {
  @Input()  emailUser: string = '';
  valueBtnNext = 'Next step';

  constructor(
    private _stepperService: StepperService,
  ) { }

  ngOnInit() { }

    goNext() {

    this._stepperService.setStepIndex(3);
  }
  goPrev() {
     this._stepperService.setStepIndex(1);
  }
}


import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { User } from '../../../models/User.models';
import { Supplier } from 'src/app/models/supplier.model';
import { StepperService } from 'src/app/services/stepper.service';
import { AuthService } from 'src/app/services/auth.service';
import { SupplierService } from 'src/app/services/supplier/supplier.service';

@Component({
  selector: 'app-profile-status',
  templateUrl: './profile-status.component.html',
  styleUrls: ['./profile-status.component.scss']
})
export class ProfileStatusComponent implements OnInit, OnDestroy {
  loading = false;
  valueBtnNext = 'Next step';
  profileStatusForm!: FormGroup;
  profileStatus = 'public';
  supplier!: Supplier;
  user!: User;
  destroySubject$: Subject<boolean> = new Subject<boolean>();

  constructor(
    private _stepperService: StepperService,
    private router: Router,
    private _AuthService: AuthService,
    private supplierService: SupplierService
  ) {}

  ngOnInit() {
   this._AuthService.authenticate().subscribe();
    this._AuthService.getConnectedUser()
      .pipe(takeUntil(this.destroySubject$)).subscribe((res) => {
        this.user = new User(res ?? {});
        this.supplier = new Supplier(this.user.supplier);
        console.log(this.supplier);
      });

  }

  goNext() {

    this.loading = true;
    this.valueBtnNext = 'Loading...';
    this.supplier.profileStatus = this.profileStatus;
    setTimeout(
      () =>
        this.supplierService
          .update(this.supplier._id, this.supplier)
          .pipe(takeUntil(this.destroySubject$))
          .subscribe(
            () => {
              this.router.navigate(['/pending-account']);
            },
            error => {
              this.loading = false;
              this.valueBtnNext = 'Next step';
            }
          ),
      400
    );
  }

  goPrev() {
    this._stepperService.setStepIndex(4);
    // this.router.navigate(['supplier-sign-up/materials']);
  }

  ngOnDestroy() {
    this.destroySubject$.next(true);
    this.destroySubject$.unsubscribe();
  }
}

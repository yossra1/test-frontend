import { Component, HostListener, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { User } from '../../models/User.models';
import { SeoService } from '../../services/seo.service';
import { SupplierService } from 'src/app/services/supplier/supplier.service';
import { StepperService } from 'src/app/services/stepper.service';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-supplier-sign-up',
  templateUrl: './supplier-sign-up.component.html',
  styleUrls: ['./supplier-sign-up.component.scss']
})
export class SupplierSignUpComponent implements OnInit, OnDestroy {
  submitted = false;

  currentStepIndex = 1;
  // steps = [1, 2, 3, 4, 5];
  steps = [1, 2, 3, 4, 5];


  public isCollapsedFashion = false;
  public isCollapsedInteriors = false;

  destroySubject$: Subject<boolean> = new Subject<boolean>();
  emailUser!: string;
  public isConnected = false;
  public user: User = new User();

  public windowWidth: number = window.innerWidth;

  constructor(
    private seoService: SeoService,
    private supplierService: SupplierService,
    private _stepperService: StepperService,
    public _AuthService: AuthService
  ) {}

  ngOnInit() {
    this.getEmail();

    this.isSuplierConnected();

    this._stepperService
      .getCurrentStep()
      .pipe(takeUntil(this.destroySubject$))
      .subscribe(res => {
        this.currentStepIndex = res;
      });

    this.seoService.generateTags({
      title: 'Supplier application',
      description: 'Supplier application',
      route: '/supplier-sign-up'
    });
  }

  getEmail() {
    this.supplierService
      .getEmail()
      .pipe(takeUntil(this.destroySubject$))
      .subscribe(email => {
        this.emailUser = email;
      });
  }

  // check if a supplier connected and must complete registration
  isSuplierConnected() {
    this._AuthService
      .getConnectedUser()
      .pipe(takeUntil(this.destroySubject$))
      .subscribe(res => {
        if (res !== null) {
          this.isConnected = true;
          this.user = res;
  
          if (
            this.user.status === 'incompleteProfile' &&
            this.user.role.name === 'supplierMaster'
          ) {
            this._stepperService.setStepIndex(3);
          }
        } else {
          this.isConnected = false;
        }
      });
  }

  // if screen size changes it'll update
  @HostListener('window:resize', ['$event'])
  resize(event: Event) {
    this.windowWidth = window.innerWidth;
  }

  ngOnDestroy() {
    this.destroySubject$.next(true);
    this.destroySubject$.unsubscribe();
  }
}

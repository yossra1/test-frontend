import { Component, OnInit } from '@angular/core';
import { SeoService } from 'src/app/services/seo.service';
import { SettingsService } from 'src/app/services/settings.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  settings = {
    facebook: '',
    instagram: '',
    linkedin: '',
    twitter: '',
    pinterest: '',
    generalEmail: '',
    pressEmail: ''
  };

  model: any = {};

  constructor(
    private settingService: SettingsService,
    private seoService: SeoService
  ) { }


  ngOnInit() {
    this.seoService.generateTags({
      title: 'Contacts',
      description: 'WHERE WE ARE HEAD OFFICE BSAMPLY INC. 11601 Wilshire Blvd, Suite 500 Los Angeles, CA 90025 – USA SEND US A MESSAGE GENERAL info@bsamply.com PRESS press@bsamply.com CONNECT WITH US',
      route: '/contacts',
    });

    this.settingService.getSetting('facebook').subscribe(res => this.settings.facebook = res.options[0].value);
    this.settingService.getSetting('instagram').subscribe(res => this.settings.instagram = res.options[0].value);
    this.settingService.getSetting('linkedin').subscribe(res => this.settings.linkedin = res.options[0].value);
    this.settingService.getSetting('twitter').subscribe(res => this.settings.twitter = res.options[0].value);
    this.settingService.getSetting('pinterest').subscribe(res => this.settings.pinterest = res.options[0].value);
    this.settingService.getSetting('generalEmail').subscribe(res => this.settings.generalEmail = res.options[0].value);
    this.settingService.getSetting('pressEmail').subscribe(res => this.settings.pressEmail = res.options[0].value);
    // this.settingService.getSetting('headOffice').subscribe(res => document.getElementById('head-office').innerHTML = res.options[0].value);
    this.settingService.getSetting('headOffice').subscribe(res => {
      const element = document.getElementById('head-office');
      if (element) {
        element.innerHTML = res.options[0].value;
      }
    });
  }

}

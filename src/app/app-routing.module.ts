import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FaceSnapListComponent } from './components/face-snap-list/face-snap-list.component'
import { LandingPageComponent } from './components/landing-page/landing-page.component';
import { LoginComponent } from './pages/login/login.component';
import { SignUpComponent } from './pages/sign-up/sign-up.component';

import { CompanyInformationsComponent } from './pages/supplier-sign-up/company-informations/company-informations.component';
import { MaterialsComponent } from './pages/supplier-sign-up/materials/materials.component';
import { SupplierSignUpComponent } from './pages/supplier-sign-up/supplier-sign-up.component';
import { ProfileStatusComponent } from './pages/supplier-sign-up/profile-status/profile-status.component';
import { ContactComponent } from './pages/contact/contact.component';



const routes: Routes = [
  { path: 'facesnaps', component: FaceSnapListComponent },
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignUpComponent },
  { path: '', component: LandingPageComponent },
  { path: 'contacts', component: ContactComponent},
  {
    path: 'supplier-sign-up',
    component: SupplierSignUpComponent,
    // canDeactivate: [CanDeactivateGuard],
    children: [
      { path: '', redirectTo: 'overview', pathMatch: 'full' },
      {
        path: 'company-information',
        component: CompanyInformationsComponent,
      },
      {
        path: 'profile-status',
        component: ProfileStatusComponent,
      },
      {
        path: 'materials',
        component: MaterialsComponent,
        // canActivate: [AuthorizationGuard]
      },
    ]
  },


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

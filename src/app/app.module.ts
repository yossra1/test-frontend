import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgbActiveModal, NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FaceSnapComponent } from './components/face-snap/face-snap.component';
import { FaceSnapListComponent } from './components/face-snap-list/face-snap-list.component';
import { HeaderComponent } from './components/header/header.component';
import { LandingPageComponent } from './components/landing-page/landing-page.component';
import { LoginComponent } from './pages/login/login.component';
import { SignUpComponent } from './pages/sign-up/sign-up.component';
import { FlashMessageComponent } from './components/flash-message/flash-message.component';


import { CompanyInformationsComponent } from './pages/supplier-sign-up/company-informations/company-informations.component';
import { MaterialsComponent } from './pages/supplier-sign-up/materials/materials.component';
import { SupplierSignUpComponent } from './pages/supplier-sign-up/supplier-sign-up.component';
import { ProfileStatusComponent } from './pages/supplier-sign-up/profile-status/profile-status.component';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxSelectModule } from 'ngx-select-ex';
import { SvgIconComponent } from './shared/svg-icon/svg-icon.component';
import { AccountInformationComponent } from './pages/supplier-sign-up/account-information/account-information.component';
import { AccountVerificationComponent } from './pages/supplier-sign-up/account-verification/account-verification.component';
import { HttpClientModule } from '@angular/common/http';
import { DialogService } from './services/dialog.service';
import { ContactComponent } from './pages/contact/contact.component';




@NgModule({
  declarations: [
    AppComponent,
    FaceSnapComponent,
    FaceSnapListComponent,
    HeaderComponent,
    LandingPageComponent,
    LoginComponent,
    FlashMessageComponent,
    SignUpComponent,
    CompanyInformationsComponent,
    MaterialsComponent,
    SupplierSignUpComponent,
    ProfileStatusComponent,
    SvgIconComponent,
    AccountInformationComponent,
    AccountVerificationComponent,
    ContactComponent,


  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    NgbModule,
    ReactiveFormsModule,
    NgxSelectModule,
    HttpClientModule,
    
  ],
  providers: [
    NgbActiveModal,
    DialogService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

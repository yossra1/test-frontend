# Use the official Node.js image as the base image
FROM node:18.10.0
# Set the working directory in the container
WORKDIR /app

# Copy package.json and package-lock.json to the working directory
COPY package*.json ./

# Install project dependencies
RUN npm install -g npm@8.19.2

# Install Angular CLI globally
RUN npm install -g @angular/cli@16.1.4


# Copy the rest of the project files to the working directory
COPY . .


# Expose port 4200
EXPOSE 4200

# Start the Angular development server
CMD ["ng", "serve", "--host", "0.0.0.0"]





